# Kaete Moto Club

Kaete Moto Club site.

## Install

```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.pip
```

## Build

```
make html
```

## Development

```
make serve
```

Access localhost:8000.

