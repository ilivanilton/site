Title: Estatuto

## Estatuto Kaeté MC

O Moto Clube Kaeté – AL, fundado em 7 de setembro de 2019 é uma associação sem fins lucrativos.
Criado no estado de Alagoas, com representatividade tanto na capital quanto no interior.
Este estatuto regerá as condutas dos seus filiados, fundadores, presidente, vice presidente e todos os demais
integrantes. Podendo abrir filiais em outros estados.

O Kaeté é um moto clube forjado no coração do nordeste, entre as belezas naturais de seu litoral e o calor do
sertão, tendo passado em todas as cidades de Alagoas, todos os fundadores e membros de escudo fechado, 
obrigatoriamente, conhecem o estado de ponta a ponta.


#### Dos Associados

**Art. 1º** - O Kaeté MC é uma entidade associativa constituída juridicamente, sem fins lucrativos, de facções
partidárias ou religiosas. Tem como objetivos sociais: 
  
  - Organizar passeios, reuniões, viagens e eventos para seus membros, famílias, amigos e visitantes;
  - Incentivar o uso correto da motocicleta de acordo com as leis de trânsito, assim como, estimular o espírito
  motociclístico de modo geral;
  - Prezar pela boa convivência e camaradagem entre grupos afins, não importando: brasão, estilo, marca ou cilindrada;
  - Desempenhar atividades de caráter beneficente e de assistência social uma vez por ano, no mínimo;
  - Comprometimento com as raízes do motociclismo: fraternidade, companheirismo, honestidade e respeito.

#### DOS ASSOCIADOS 

**Art. 2º** - São considerados membros do Kaeté MC pessoas físicas que tenham sido previamente aprovadas pelos
conselho de fundadores e se enquadram nas normas do estatuto seguindo a hierarquia: 

  - I - fundadores; 
  - II -  escudo fechado; 
  - III - meio escudos; 
  - IV - prospectos; 
  - V - honorários; 
  - VI - atrelados. 

I - São fundadores, exclusivamente, os que participaram do ato de fundação do Moto Clube e subscreveram o Estatuto
Social. Eles fazem parte da diretoria, também se enquadram como membros efetivos. 

II - Os membros de escudo fechado são associados com escudo fechado.

III - Os meios escudos são associados em avaliação que usam metade dos patchs do Kaeté Mc.

IV - Os prospectos são associados em avaliação que usam identificação do Kaeté Mc e frequentam o
círculo de membros efetivos. 

V - Os honorários são aqueles a quem o brasão for concedido com pleno consenso dos fundadores como homenagem por
serviços prestados ao Kaeté MC, ao motociclismo ou a sociedade. 

VI - Os atrelados são: esposas, esposos e filhos de membros escudo fechado. Podendo estes, utilizar o brasão do moto
clube. Os membros de escudo fechado respondem pelos quaisquer atos de seus atrelados.
 
**Art. 3º** - O motoqueiro pode ter acesso ao moto clube como visitante através de indicação de um membro, bem como,
que de alguma forma tenha tomado conhecimento e por iniciativa própria entrou em contato com os Kaetés. Diante disto
para ser admitido como prospecto é necessário cumprir com as seguintes condições:

  - I - aceitar e submeter-se ao Estatuto Social do Kaeté MC e demais normas, declarando assim, identificar-se com o
objetivo social da entidade;
  - II - ter habilitação para condução de motocicletas;
  - III - ser motociclista; 
  - IV - apresentar nada consta estadual e federal;
  - V - passar declaração que desobriga o Kaeté MC de quaisquer responsabilidades por seus atos;
  - VI - frequentar o meio social do clube como visitante e rodar com os membros algumas vezes;
  - VII - ter aprovação do crivo unânime do conselho.

 
**PARÁGRAFO ÚNICO** - O padrinho será definido pelo conselho, sendo esse um membro de escudo fechado. Cada padrinho
pode ter no máximo dois afilhados por vez (prospecto e/ou meio escudo) e será como conselheiro para ele.

**Art. 4º** - São condições de ascensão no Kaeté MC:

  - I - O visitante passa a ser prospecto, por critérios de conduta e rito de acordo o artigo 3º;
  - II - O prospecto passa a ser membro de meio escudo, por critérios de conduta e rito;
  - III - O membro de meio escudo da matriz “Alagoas”, passa a ser membro de escudo fechado, por critérios de
  conduta e rito. Assim como, obrigatoriamente ter passado por todos os municípios de Alagoas, registrando sua
  passagem com sua moto em cada prefeitura desses municípios.
  Como também, tempo mínimo de seis meses na categoria meio escudo e um ano dentro do moto clube;
  - IV - O membro de meio escudo da Subsede passa a ser membro de escudo fechado, por critérios de conduta e
  rito. Assim como, obrigatoriamente ter passado pelos 102 municípios do seu estado, registrando sua passagem com sua
  moto em cada prefeitura desses municípios. Como também, tempo mínimo de seis meses na categoria meio escudo e um 
  ano dentro do moto clube;
  - V - Membros honorários são regidos pelo artigo 2º.
 
#### DOS DIREITOS E DEVERES 

**Art. 5º** - Os Fundadores tem direito de:

  - I - votar para cargos de presidente e vice; 
  - II - votar alterações no estatuto;
  - III - votar para entrada de novo associado;
  - IV - renunciar ou se abster de mandatos.

**§ 1.º** Todo membro de escudo fechado tem direito: 

  - I - usar o escudo do moto clube;
  - II - votar sobre assuntos colocados em pauta pelo conselho/diretoria;
  - III - tirar licença por motivos de interesse particular, por tempo indeterminado. Desde que, durante o período
  de licença, não ingresse em nenhum outro moto clube; 
  - IV - o membro que estiver afastado, por motivo de licença de interesse particular, pode frequentar festas ou
  eventos do clube. Desde que haja comunicação prévia à diretoria, assim como pagamento de uma taxa corrigida
  referente às despesas do mesmo;
  - V - participar de todos os eventos do moto clube;
  - VI - examinar a qualquer tempo e sem prévio aviso os livros, balancetes, balanços, contas, documentos, bem como
  solicitar informações de natureza econômico-financeira e patrimonial;
  - VII - apresentar-se com suas(seus) acompanhantes ou companheiras(os) ou convidadas(os);
  - VIII - renunciar ou se abster a cargos;
  - IX - desligar-se da associação;
  - X - ser tratado de forma digna, sem distinções.

**§ 2.º** Dos meios escudos e prospectos: 

  - I - usar patchs de sua graduação;
  - II - participar de todos eventos do motoclube;
  - III - ser tratado de forma digna, sem discriminação.

**§ 3.º** Dos prospectos: 

  - I - usar patchs de sua graduação;
  - II - participar de todos eventos do motoclube;
  - III - ser tratado de forma digna, sem discriminação.

**Art 5°** - São deveres dos associados: 

  - I - cumprir todas as disposições deste Estatuto social;
  - II - comparecer às reuniões, que serão feitas mensalmente, de forma oficial. Não implicando em reuniões
  informais no intervalo entre uma reunião e outra;
  - III - usar o escudo do Kaeté MC sempre que possível no dia a dia, sendo obrigatório o uso em: viagens,
  encontros, reuniões e qualquer evento que envolva o motociclismo, desempenhando, ainda, fielmente as funções
  para a qual tenha sido indicado;
  - IV - prestar ajuda aos demais membros do clube, sempre que possível;
  - V - não faltar mais de 1/3 das reuniões do clube, assim como eventos confirmados previamente pelo moto clube;
  - VI - o pagamento de mensalidade é obrigatório, regido pelo artigo 12º;
  - VII - não externar assuntos internos do clube;
  - VIII - compromisso com o Kaeté MC, consideramos a hierarquia de prioridades: Família, trabalho e moto clube;
  - IX - zelar pela preservação do patrimônio moral e material, pelos bons costumes, entre associados ou não,
  eximindo-se de quaisquer práticas que possa denegrir a imagem e o bom nome do moto clube e os seus associados.
  - X - responsabilizar-se por atos, atitudes, comportamento ou danos praticados por si, suas(seus) acompanhantes ou
  convidadas(os), desonerando sempre o moto clube; 
 
**1º** - Caso o membro seja desligado do clube, é obrigatória a devolução do escudo do clube, assim como qualquer
outra peça de identificação do clube.

**2º** - Membros que não rodam não tem direito a voto.

**3º** - O voto de Minerva é definido pela maioria dos votos dos Fundadores,
caso ocorra empate logo o voto do presidente será o decissório.

**Art 7°** - Extingue-se a condição de associado:

  - I – por solicitação espontânea do próprio;
  - II – por aplicação da penalidade de exclusão, na forma do Estatuto Social;
  - III – por morte.
 
**PARÁGRAFO ÚNICO** - Na hipótese do inciso II caberá recurso com efeito suspensivo à primeira assembléia geral que ocorrer após o fato. 
 
#### DAS PENALIDADES

**Art 8º** - Serão aplicáveis as seguintes penalidades:

  - I – advertência disciplinar, por:
    - Desrespeito ou desacato para com qualquer um dos integrantes do moto clube;
    - Promover ou participar de quaisquer atos que atentam contra a ordem legal e com os interesses do moto clube,
    ou não utilizar com galhardia as cores e indicativos do Kaeté;
    - Qualquer ato de desrespeito do membro para com sua própria família, já que baseamos o Kaeté MC na integração
    familiar dos membros, formando uma grande família.
  - II – suspensão, por:
    - Reincidência da penalidade prevista no inciso anterior;
    - Inobservância de quaisquer um dos dispositivos previstos nos atos normativos do moto clube;
    - O membro acusado por crimes dolosos e afins, enquanto durar o processo criminal;
    - Qualquer postura ou ação que demonstre falta de respeito à família de um membro, visto que, tratamos todos de
    forma igual, independente de orientação sexual, tipo familiar e afins.
  - III – Expulsão, por:
    - Promover ou participar, direta ou indiretamente, de atos atentatórios à dignidade social, do motociclismo ou
    da vida em irmandade;
    - Incapacidade civil não suprida;
    - Quaisquer outros motivos devidamente fundamentados e por requerimento formulado por quaisquer associado,
    se aprovado por dois terços dos votos de escudo fechado presentes em Assembléia Geral.
    - Sentença judicial com trânsito em julgado, em casos de crimes dolosos e afins, acarretará em desligamento
    do membro.

**1°** - As penalidades poderão ser aplicadas por quaisquer dos diretores, vedada a cumulatividade entre eles e
executadas mediante ofício pela Diretoria.

**2°** - Na hipótese do inciso II, o diretor que aplicar a penalidade deverá fixar o prazo da punição.

**3º** - No ato da suspensão o associado tem que entregar seu colete ao diretor que só será devolvido ao término
da suspensão.
 
#### DO BRASÃO E COLETES DO KAETE MC

**Art 9º** - Os símbolos e organização quanto às vestes do Kaeté MC são:

  - I – Brasão: Consiste em uma caveira, com penas formando um cocar;
  - II - Das cores: As cores oficiais são Preto e Branco;
  - III - Coletes: Em couro, apenas com o brasão do Kaeté MC no peito do lado esquerdo. O lado direito do peito
  está reservado para a identificação do membro e seu tipo sanguíneo, assim como identificação interna do clube:
    - Associados de colete fechado são distinguidos pelo patch com o nome KAETÉ MC, seguido da caveira símbolo e
    o patch do estado do associado;
    - Associados de meio escudo são distinguidos pelo patch com o nome KAETÉ MC e o patch do estado do associado;
    - Associados prospectos são distinguidos pelas costas lisas;
  - IV - Patchs, insígnias, emblemas e afins, apenas serão autorizados se forem oriundos de mérito, desafios ou
  honrarias. 
 
#### DAS COMPETÊNCIAS, PATRIMÔNIO E ORGANIZAÇÃO ADMINISTRATIVA

**Art 10º** - Compete ao presidente: 

  - I - Representar o moto clube Juridicamente;
  - II - Representar o moto clube em reuniões e eventos externos;
  - III - Presidir as reuniões e assembleias;
  - IV - Representar o moto clube junto a empresas públicas e privadas, entidades, moto clubes, etc;
  - V - Assinar, juntamente com um Diretor, qualquer contrato referente ao item acima;
  - VI - Tomar decisões em conjunto com o conselho de fundadores.
 
**Art 11º** - Compete ao vice-presidente:

  - I - Na ausência do Presidente, praticar qualquer ato de sua competência;
  - II - Convocar assembleias gerais, ordinárias e/ou extraordinárias;
  - IV - auxiliar o presidente em todos os atos de sua competência.

**Art 12º** - Compete aos fundadores: 

  - I - Serem elegíveis aos cargos de: presidente, vice-presidente e demais cargos do moto clube;
  - II -  Trabalhar em conjunto com o presidente e vice-presidente, constituindo um crivo decisivo;
  - III - Dar suporte a todos os membros do clube, sem distinção alguma.

**Art 13º** - Compete ao Secretário:

  - I - Elaborar e executar o processo seletivo do moto clube, criar as ferramentas que propiciem a melhoria
  contínua nas condições dos integrantes, avaliar o desempenho dos prospectos, elaborar atas das reuniões
  administrativas e assembleias, encaminhamento de e-mail, controle de ficha individual.
 
**Art 14º** - Compete ao Diretor Financeiro:

  - I - Ter sob sua guarda e responsabilidade toda documentação de caráter financeiro do moto clube;
  - II - Efetuar as despesas do moto clube, monitorando compras e vendas quando devidamente aprovado em assembleia;
  - III - Assinar junto com presidente os cheques e demais documentos de responsabilidade financeira;
  - IV - Promover a arrecadação das mensalidades;
  - V - Dirige os serviços de contabilidade e organiza o orçamento anual a ser apresentado a diretoria, controla
  movimentações contábeis e financeiras.
 
**Art 15º** - Compete ao Diretor de Disciplina:

  - I - Caberá ao Diretor de Disciplina comunicar aos conselheiros e a diretoria, todas as faltas que os membros
  associados cometerem, no seio da associação ou fora desta, para que possa ser instaurado o procedimento
  administrativo, facultando ao interessado todos os meios de defesas pertinentes, sempre observando o Princípio
  da Ampla Defesa e do Contraditório, a fim de julgar cada caso concreto. Caberá ainda ao Diretor de Disciplina,
  em conjunto com conselho, após a apresentação de todas as provas acostadas ao procedimento administrativo julgar
  todos os atos, infrações ou faltas praticados pelos membros associados.
 
**Art 16º** - Compete ao Capitão de Estrada:

  - I - Definir e coordenar formação de passeio de múltipla moto;
  - II - Delegar funções no comboio;
  - III - Verificar a segurança dos pilotos;
  - IV - Planejar roteiros e viagens e mesmo servir como guia de turismo.
 
**Art. 17º** - Disposições sobre as mensalidades: 

  - I - Todos associados, fundadores, presidente e vice-presidente tem o dever de pagar as mensalidades;
    - Membros honorários e atrelados não pagam. 
  - II - O valor da mensalidade é determinado na assembleia geral;
  - III - A quantia arrecadada será utilizada para confecção de adesivos, bandeiras, ações sociais e demais
  necessidades do Kaeté MC, dependendo do bom senso e deliberado em assembleia ou reuniões extraordinárias;
  - IV - A prestação de contas será irrestrita para os membros, exposta nas reuniões oficiais, assim como em
  demais canais de comunicação;
  - V - O membro não poderá acumular mais de 3 meses sem pagamento;
  - VI - Há a figura do tesoureiro, responsável pela organização das finanças do clube.
 
**Art 18º** - Constitui patrimônio do clube:

  - I - Brasões e quaisquer identificações são de propriedade do Kaeté MC, devendo o membro entregar à diretoria
  em caso de desligamento; 
  - II - Bandeira e demais objetos de representação do clube. 

#### DA SEDE

**Art. 19º** Sobre a sede:

  - Aviso prévio do uso da sede;
  - Chave da sede somente para escudos fechados;
  - Uso restrito apenas nas áreas externas;
  - Limpeza semanal de acordo com escale previamente definida;
  - Proibido o uso para fins externos sem a devida autorização.

#### DAS SUBSEDES

**Art. 20º** - Sobre subsedes e membros de outros estados:

  - I - Subsedes só serão criadas caso o estado tenha pelo menos 1 (um) membro de escudo fechado, 1 (um) meio escudo e pelo menos 4 (quatro) membros. Sendo delegado a diretoria de seu estado por crivo unânime dos fundadores;
  - II - Os membros das subsedes votarão a inclusão e exclusão de futuros membros que estão atrelados a sua subsede.

**Art. 21º** - O presente Código poderá sempre ser acrescido ou modificado a critério de comissão formada pelos fundadores, atualizado em 13 de setembro de 2020.
