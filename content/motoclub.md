Title: Motoclube
Date: 2019-9-7
Category: Motoclube
Tag: Kaeté

**[**Kaeté Moto Clube**](http://kaetemoto.club/pages/kaete.html)** é um moto 
clube brasileiro fundado em 2019 no estado de **Alagoas**.

Os Motoclubes (*Motoclub*) são constituídos e organizados por pessoas que 
apreciam o motociclismo. 
São clubes organizados com a finalidade de estabelecer relações de camaradagem 
e amizade, promovendo a socialização entre seus participantes.

Durante todo o ano, os moto clubes organizam passeios e encontros para promover
a confraternização, a amizade e o companheirismo entre todos os seus 
componentes.
